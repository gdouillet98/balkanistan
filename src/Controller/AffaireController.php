<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Affaire;
use App\Entity\Politicien;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 

use App\Form\Type\AffaireType;
use App\Form\Type\AffaireCreateType;
use App\Form\Type\AffaireAjoutPolType;

class AffaireController extends AbstractController {
    public function accueil(Session $session) {

        $affaires = $this->getDoctrine()->getRepository(Affaire::class)->findAll();
        
        return $this->render('affaire/accueil.html.twig', array('affaires' => $affaires));
    }

    public function voir($id) {
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($id);
        if(!$affaire)
            throw $this->createNotFoundException('Affaire[id='.$id.'] inexistante');
        return $this->render('affaire/voir.html.twig',array('affaire' => $affaire));
    }

    public function ajouter2(Request $request) {
        $affaire = new Affaire;
        $form = $this->createForm(AffaireType::class, $affaire);
        $form->add('submit', SubmitType::class,
                array('label' => 'Ajouter'));
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($affaire);
            $entityManager->flush(); 
            return $this->redirectToRoute('affaire_voir', array('id' => $affaire->getId()));
        }
        return $this->render('affaire/ajouter2.html.twig',
        array('monFormulaire' => $form->createView()));
    }

    public function modifier($id) {
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($id);
        if(!$affaire)
            throw $this->createNotFoundException('Affaire[id='.$id.'] inexistante');
        $form = $this->createForm(AffaireType::class, $affaire, ['action' => $this->generateUrl('affaire_modifier_suite', array('id' => $affaire->getId()))]);
        $form->add('submit', SubmitType::class, array('label' => 'Modifier'));
        return $this->render('affaire/modifier.html.twig', array('monFormulaire' => $form->createView()));
    }

    public function modifierSuite(Request $request, $id) {
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($id);
        if(!$affaire)
            throw $this->createNotFoundException('Affaire[id='.$id.'] inexistante');
        $form = $this->createForm(AffaireType::class, $affaire,
        ['action' => $this->generateUrl('affaire_modifier_suite',
        array('id' => $affaire->getId()))]);
        $form->add('submit', SubmitType::class, array('label' => 'Modifier'));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($affaire);
            $entityManager->flush();
            $url = $this->generateUrl('affaire_voir', array('id' => $affaire->getId()));
            return $this->redirect($url);
        }
        return $this->render('affaire/modifier.html.twig', array('monFormulaire' => $form->createView()));
    }

    public function ajoutPol($id) {
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($id);
        if(!$affaire)
            throw $this->createNotFoundException('Affaire[id='.$id.'] inexistante');
        $form = $this->createForm(AffaireAjoutPolType::class, $affaire, ['action' => $this->generateUrl('affaire_ajoutPol_suite', array('id' => $affaire->getId()))]);
        $form->add('submit', SubmitType::class, array('label' => 'Ajouter'));
        return $this->render('affaire/ajoutPol.html.twig', array('monFormulaire' => $form->createView()));
    }

    public function ajoutPolSuite(Request $request, $id) {
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($id);
        if(!$affaire)
            throw $this->createNotFoundException('Affaire[id='.$id.'] inexistante');
        $form = $this->createForm(AffaireAjoutPolType::class, $affaire,
        ['action' => $this->generateUrl('affaire_ajoutPol_suite',
        array('id' => $affaire->getId()))]);
        $form->add('submit', SubmitType::class, array('label' => 'Ajouter'));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($affaire);
            $entityManager->flush();
            $url = $this->generateUrl('affaire_voir', array('id' => $affaire->getId()));
            return $this->redirect($url);
        }
        return $this->render('affaire/ajoutPol.html.twig', array('monFormulaire' => $form->createView()));
    }


    public function supprimer(Request $request, $id) {
        $em = $this->getDoctrine()->getManager(); 
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($id);
        $listPol = $affaire->getPoliticiens();
        if(!$affaire)
            throw $this->createNotFoundException('Affaire[id='.$id.'] inexistante');
        
        if($listPol->isEmpty()){
            $em->remove($affaire);
            $em->flush(); 
            return $this->redirectToRoute('affaire_accueil', array('id' => $affaire->getId()));
        }else{
            throw $this->createNotFoundException('Affaire contient des candidats');
        }
    }

    public function SuppPol(Request $request, $id1, $id2) {
        $em = $this->getDoctrine()->getManager(); 
        $politicien = $this->getDoctrine()->getRepository(Politicien::class)->find($id1);
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($id2);
        if(!$politicien)
            throw $this->createNotFoundException('Affaire[id='.$id.'] inexistante');

        $affaire->removePoliticien($politicien);
        $em->flush(); 
        return $this->redirectToRoute('affaire_accueil');
    }
}