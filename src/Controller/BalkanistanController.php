<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Mairie;
use App\Entity\Affaire;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 
use App\Form\Type\MairieType;
use App\Controller\AffaireController;


class BalkanistanController extends AbstractController
{
    /**
     * @Route("/balkanistan", name="balkanistan")
     */
    public function accueil()
    {
        return $this->render('balkanistan/accueil.html.twig');
    }

    /**
     * @Route("/recherche", name="search_result", methods={"GET"})
     */
    public function search(Request $request): Response
    {

        
        $search = $request->query->all();


        $res = $this->getDoctrine()->getRepository(Affaire::class)->findAllByDesignation($search["search"]);

        return $this->render('affaire/accueil.html.twig', [
            'affaires' => $res,
        ]);
    }

}
