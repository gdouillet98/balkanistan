<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Parti;
use App\Entity\Politicien;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 
use App\Form\Type\PartiType;

class PartiController extends AbstractController {
    public function accueil(Session $session) {
        if ($session->has('nbreFois'))
            $session->set('nbreFois', $session->get('nbreFois')+1);
        else
            $session->set('nbreFois', 1);

        $partis = $this->getDoctrine()->getRepository(Parti::class)->findAll();
        
        return $this->render('parti/accueil.html.twig', array('partis' => $partis));
    }

    public function voir($id) {
        $parti = $this->getDoctrine()->getRepository(Parti::class)->find($id);
        $listPol = $this->getDoctrine()->getRepository(Politicien::class)->findBy(array("parti" => $id));
        if(!$parti)
            throw $this->createNotFoundException('Parti[id='.$id.'] inexistante');
        return $this->render('parti/voir.html.twig',
                             ['parti' => $parti,
                             'politiciens' => $listPol]);
    }

    public function ajouter2(Request $request) {
        $parti = new Parti;
        $form = $this->createForm(PartiType::class, $parti);
        $form->add('submit', SubmitType::class,
                array('label' => 'Ajouter'));
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($parti);
            $entityManager->flush(); 
            return $this->redirectToRoute('parti_voir', array('id' => $parti->getId()));
        }
        return $this->render('parti/ajouter2.html.twig',
        array('monFormulaire' => $form->createView()));
    } 

    public function modifier($id) {
        $parti = $this->getDoctrine()->getRepository(Parti::class)->find($id);
        if(!$parti)
            throw $this->createNotFoundException('Parti[id='.$id.'] inexistante');
        $form = $this->createForm(PartiType::class, $parti, ['action' => $this->generateUrl('parti_modifier_suite', array('id' => $parti->getId()))]);
        $form->add('submit', SubmitType::class, array('label' => 'Modifier'));
        return $this->render('parti/modifier.html.twig', array('monFormulaire' => $form->createView()));
    }

    public function modifierSuite(Request $request, $id) {
        $parti = $this->getDoctrine()->getRepository(Parti::class)->find($id);
        if(!$parti)
            throw $this->createNotFoundException('Parti[id='.$id.'] inexistante');
        $form = $this->createForm(PartiType::class, $arti,
        ['action' => $this->generateUrl('parti_modifier_suite',
        array('id' => $parti->getId()))]);
        $form->add('submit', SubmitType::class, array('label' => 'Modifier'));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($parti);
            $entityManager->flush();
            $url = $this->generateUrl('parti_voir', array('id' => $parti->getId()));
            return $this->redirect($url);
        }
        return $this->render('parti/modifier.html.twig', array('monFormulaire' => $form->createView()));
    }

    public function supprimer(Request $request, $id) {
        $em = $this->getDoctrine()->getManager(); 
        $parti = $this->getDoctrine()->getRepository(Parti::class)->find($id);
        $listPol = $this->getDoctrine()->getRepository(Politicien::class)->findBy(array("parti" => $id));
        if(!$parti)
            throw $this->createNotFoundException('Parti[id='.$id.'] inexistante');
        
        if(!$listPol){
            $em->remove($parti);
            $em->flush(); 
            return $this->redirectToRoute('parti_accueil', array('id' => $parti->getId()));
        }else{
            throw $this->createNotFoundException('Parti contient des politiciens');
        }
    }

    public function SuppPol(Request $request, $id1, $id2) {
        $em = $this->getDoctrine()->getManager(); 
        $politicien = $this->getDoctrine()->getRepository(Politicien::class)->find($id1);
        $parti = $this->getDoctrine()->getRepository(Parti::class)->find($id2);
        if(!$politicien)
            throw $this->createNotFoundException('parti[id='.$id.'] inexistante');

        $parti->removePoliticien($politicien);
        $em->flush(); 
        return $this->redirectToRoute('parti_accueil');
    }
}