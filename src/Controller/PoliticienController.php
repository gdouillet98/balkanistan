<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Politicien;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 
use App\Entity\Mairie;
use App\Entity\Parti;
use App\Entity\Affaire;
use App\Form\Type\PoliticienType;
use App\Form\Type\PolModifType;

class PoliticienController extends AbstractController {
    public function accueil(Session $session) {
        
        $politiciens = $this->getDoctrine()->getRepository(Politicien::class)->findAll();
        
        return $this->render('politicien/accueil.html.twig', array('politiciens' => $politiciens));
    }

    public function voir($id) {
        $politicien = $this->getDoctrine()->getRepository(Politicien::class)->find($id);

        if(!$politicien)
            throw $this->createNotFoundException('Politicien[id='.$id.'] inexistante');
        return $this->render('politicien/voir.html.twig', array('politicien' => $politicien));
    }

    public function ajouter2(Request $request) {
        $politicien = new Politicien;
        $form = $this->createForm(PoliticienType::class, $politicien);
        $form->add('submit', SubmitType::class,
                array('label' => 'Ajouter'));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($politicien);
            $entityManager->flush(); 
            return $this->redirectToRoute('politicien_voir', array('id' => $politicien->getId()));
        }
        return $this->render('politicien/ajouter2.html.twig',
        array('monFormulaire' => $form->createView()));
    }
    
    public function modifier($id) {
        $politicien = $this->getDoctrine()->getRepository(Politicien::class)->find($id);
        if(!$politicien)
            throw $this->createNotFoundException('Politicien[id='.$id.'] inexistante');
        $form = $this->createForm(PolModifType::class, $politicien, ['action' => $this->generateUrl('politicien_modifier_suite', array('id' => $politicien->getId()))]);
        $form->add('submit', SubmitType::class, array('label' => 'Modifier'));
        return $this->render('politicien/modifier.html.twig', array('monFormulaire' => $form->createView()));
    }

    public function modifierSuite(Request $request, $id) {
        $politicien = $this->getDoctrine()->getRepository(Politicien::class)->find($id);
        if(!$politicien)
            throw $this->createNotFoundException('Politicien[id='.$id.'] inexistante');
        $form = $this->createForm(PolModifType::class, $politicien,
        ['action' => $this->generateUrl('politicien_modifier_suite',
        array('id' => $politicien->getId()))]);
        $form->add('submit', SubmitType::class, array('label' => 'Modifier'));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($politicien);
            $entityManager->flush();
            $url = $this->generateUrl('politicien_voir', array('id' => $politicien->getId()));
            return $this->redirect($url);
        }
        return $this->render('politicien/modifier.html.twig', array('monFormulaire' => $form->createView()));
    }

    public function supprimer(Request $request, $id) {
        $em = $this->getDoctrine()->getManager(); 
        $politicien = $this->getDoctrine()->getRepository(Politicien::class)->find($id);
        if(!$politicien)
            throw $this->createNotFoundException('Politicien[id='.$id.'] inexistante');
        $em->remove($politicien);
        $em->flush(); 
        return $this->redirectToRoute('politicien_accueil', array('id' => $politicien->getId()));
    }
}