<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PoliticienRepository")
 */
class Politicien
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=45)
     * * @Assert\Regex(pattern="/^(?:M|F)$/",
     * message="Veuillez entrer M ou F")
     */
    private $sexe;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\GreaterThan(value = 18,
     * message="la valeur doit être > {{ compared_value }}")
     */
    private $age;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Mairie", inversedBy="politiciens")
     */
    private $candidat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Parti", inversedBy="politiciens")
     */
    private $parti;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Affaire", mappedBy="politiciens")
     * @ORM\JoinColumn(nullable=true)
     */
    private $implication;

   

    public function __construct()
    {
        $this->IsCase = new ArrayCollection();
        $this->implication = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getCandidat(): ?Mairie
    {
        return $this->candidat;
    }

    public function setCandidat(?Mairie $candidat): self
    {
        $this->candidat = $candidat;

        return $this;
    }

    public function getParti(): ?Parti
    {
        return $this->parti;
    }

    public function setParti(?Parti $parti): self
    {
        $this->parti = $parti;

        return $this;
    }

    /**
     * @return Collection|Affaire[]
     */
    public function getImplication(): Collection
    {
        return $this->implication;
    }

    public function setImplication($implication) {

        if (!$this->implication->contains($implication)) {
            $this->implication[] = $implication;
        }
        
        return $this;
        
    }

    public function addImplication(Affaire $implication): self
    {
        if (!$this->implication->contains($implication)) {
            $this->implication[] = $implication;
        }

        return $this;
    }

    public function removeImplication(Affaire $implication): self
    {
        if ($this->implication->contains($implication)) {
            $this->implication->removeElement($implication);
        }

        return $this;
    }

    public function __toString(){
        return $this->getNom();
    }

}
