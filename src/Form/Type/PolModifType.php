<?php
namespace App\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Parti;
use App\Entity\Affaire;
use App\Entity\Mairie;
use App\Entity\Politicien;


class PolModifType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('sexe', TextType::class)
                ->add('age', TextType::class)
                ->add('candidat', EntityType::class,
                array('class' => Mairie::class))
                ->add('parti', EntityType::class,
                array('class' => Parti::class));
                
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
        'data_class' => Politicien::class,
        ));
    }
}