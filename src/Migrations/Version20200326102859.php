<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200326102859 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE affaire ADD politicien_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE affaire ADD CONSTRAINT FK_9C3F18EF7C1FA7B6 FOREIGN KEY (politicien_id) REFERENCES politicien (id)');
        $this->addSql('CREATE INDEX IDX_9C3F18EF7C1FA7B6 ON affaire (politicien_id)');
        $this->addSql('ALTER TABLE politicien ADD is_candidate_id INT DEFAULT NULL, ADD is_in_parti_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE politicien ADD CONSTRAINT FK_D7F73E4DB103645F FOREIGN KEY (is_candidate_id) REFERENCES mairie (id)');
        $this->addSql('ALTER TABLE politicien ADD CONSTRAINT FK_D7F73E4DE7BA56EF FOREIGN KEY (is_in_parti_id) REFERENCES parti (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D7F73E4DB103645F ON politicien (is_candidate_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D7F73E4DE7BA56EF ON politicien (is_in_parti_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE affaire DROP FOREIGN KEY FK_9C3F18EF7C1FA7B6');
        $this->addSql('DROP INDEX IDX_9C3F18EF7C1FA7B6 ON affaire');
        $this->addSql('ALTER TABLE affaire DROP politicien_id');
        $this->addSql('ALTER TABLE politicien DROP FOREIGN KEY FK_D7F73E4DB103645F');
        $this->addSql('ALTER TABLE politicien DROP FOREIGN KEY FK_D7F73E4DE7BA56EF');
        $this->addSql('DROP INDEX UNIQ_D7F73E4DB103645F ON politicien');
        $this->addSql('DROP INDEX UNIQ_D7F73E4DE7BA56EF ON politicien');
        $this->addSql('ALTER TABLE politicien DROP is_candidate_id, DROP is_in_parti_id');
    }
}
